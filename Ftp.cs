﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Budziszewski.Net
{
    public abstract class Ftp
    {
        public string Hostname { get; protected set; }
        public NetworkCredential Credentials { get; protected set; }

        public Ftp(string hostname, NetworkCredential credentials)
        {
            Hostname = hostname;
            Credentials = credentials;
        }

        public async Task<object> DeserializeAsync(string remotePath)
        {
            return await Task.Run(() => Deserialize(remotePath));
        }

        public abstract object Deserialize(string remotePath);

        public async Task<bool> SerializeAsync(string remotePath, object obj, bool safely = false, int backups = 0)
        {
            return await Task.Run(() => Serialize(remotePath, obj, safely, backups));
        }

        public abstract bool Serialize(string remotePath, object obj, bool safely = false, int backups = 0);

        protected string GetSafeTempPath(string uri)
        {
            int index = 0;
            while (FileExists(uri + ".TEMPORARY_" + index))
            {
                index++;
            }
            return uri + ".TEMPORARY_" + index;
        }

        public async Task<bool> UploadFileAsync(string remotePath, string localPath, bool safely = false, int backups = 0)
        {
            return await Task.Run(() => UploadFile(remotePath, localPath, safely, backups));
        }

        public abstract bool UploadFile(string remotePath, string localPath, bool safely = false, int backups = 0);

        public async Task<bool> DownloadFileAsync(string remotePath, string localPath)
        {
            return await Task.Run(() => DownloadFile(remotePath, localPath));
        }

        public abstract bool DownloadFile(string remotePath, string localPath);

        public abstract bool FileExists(string remotePath);

        public abstract bool DirectoryExists(string remotePath);

        public abstract bool Rename(string remotePath, string renameTo);

        public abstract bool DeleteFile(string remotePath);

        public abstract bool CreateDirectory(string remotePath);

        public abstract IEnumerable<string> FindFiles(string remotePath, string pattern = "*");

        protected void CleanUpBackups(string remotePath, int backups)
        {
            var files = FindFiles(remotePath.Substring(0, remotePath.LastIndexOf('/')), remotePath.Substring(remotePath.LastIndexOf('/') + 1) + ".BACKUP_*");

            var filesToRemove = files.OrderByDescending(x => x).Skip(backups);

            foreach (var f in filesToRemove)
            {
                DeleteFile(remotePath.Substring(0, remotePath.LastIndexOf('/')) + "/" + f);
            }
        }

        public delegate void FtpProgressHandler(FtpProgressEventArgs e);
        public event FtpProgressHandler FtpProgress;
        protected void OnFtpProgress(FtpProgressEventArgs e)
        {
            FtpProgress?.Invoke(e);
        }

        public class FtpProgressEventArgs : EventArgs
        {
            public string RemotePath { get; private set; }
            public string LocalPath { get; private set; }
            public double Progress { get; private set; }

            public FtpProgressEventArgs(string remotePath, string localPath, double progress) : base()
            {
                RemotePath = remotePath;
                LocalPath = localPath;
                Progress = progress;
            }
        }
    }

    public class LegacyFtp : Ftp
    {
        public LegacyFtp(string hostname, NetworkCredential credentials) : base(hostname, credentials)
        {
            if (!Hostname.StartsWith("ftp://")) Hostname = "ftp://" + Hostname;
        }

        public override object Deserialize(string remotePath)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(Hostname.TrimEnd('/') + "/" + remotePath, UriKind.RelativeOrAbsolute));
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = Credentials;

            using (Stream s = request.GetResponse().GetResponseStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                return bf.Deserialize(s);
            }
        }

        public override bool Serialize(string remotePath, object obj, bool safely = false, int backups = 0)
        {
            string uri = remotePath;
            try
            {
                if (safely)
                {
                    uri = GetSafeTempPath(uri);
                }

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(Hostname.TrimEnd('/') + "/" + uri, UriKind.RelativeOrAbsolute));
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = Credentials;

                using (Stream s = request.GetRequestStream())
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    bf.Serialize(s, obj);
                }

                // Uploaded safely
                if (safely)
                {
                    if (backups > 0) Rename(remotePath, remotePath.Substring(remotePath.LastIndexOf('/') + 1) + ".BACKUP_" + DateTime.Now.ToString("yyyyMMddHHmmss", CultureInfo.InvariantCulture));
                    Rename(uri, remotePath.Substring(remotePath.LastIndexOf('/') + 1));
                    CleanUpBackups(remotePath, backups);
                }
                return true;
            }
            catch
            {
                DeleteFile(uri);
                return false;
            }
        }

        public override bool UploadFile(string remotePath, string localPath, bool safely = false, int backups = 0)
        {
            string uri = remotePath;
            try
            {
                if (safely)
                {
                    uri = GetSafeTempPath(uri);
                }

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(Hostname.TrimEnd('/') + "/" + uri, UriKind.RelativeOrAbsolute));
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = Credentials;
                request.KeepAlive = false;
                request.UseBinary = true;
                request.UsePassive = true;

                using (Stream s = request.GetRequestStream())
                {
                    using (FileStream fs = File.Open(localPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        int bytesRead = 0;
                        int bytesTotal = (int)fs.Length;

                        byte[] buffer = new byte[1024];
                        int read;
                        while ((read = fs.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            s.Write(buffer, 0, read);
                            bytesRead += read;
                            OnFtpProgress(new FtpProgressEventArgs(remotePath, localPath, (double)bytesRead / (double)bytesTotal));
                        }
                    }
                }

                // Uploaded safely
                if (safely)
                {
                    if (backups > 0) Rename(remotePath, remotePath.Substring(remotePath.LastIndexOf('/') + 1) + ".BACKUP_" + DateTime.Now.ToString("yyyyMMddHHmmss", CultureInfo.InvariantCulture));
                    Rename(uri, remotePath.Substring(remotePath.LastIndexOf('/') + 1));
                    CleanUpBackups(remotePath, backups);
                }
                return true;
            }
            catch
            {
                DeleteFile(uri);
                return false;
            }
        }

        public override bool DownloadFile(string remotePath, string localPath)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(Hostname.TrimEnd('/') + "/" + remotePath, UriKind.RelativeOrAbsolute));
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = Credentials;
                request.KeepAlive = false;
                request.UseBinary = true;
                request.UsePassive = true;

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                using (Stream s = response.GetResponseStream())
                {
                    using (FileStream fs = File.Create(localPath))
                    {
                        int bytesRead = 0;
                        int bytesTotal = (int)s.Length;

                        byte[] buffer = new byte[1024];
                        int read;
                        while ((read = s.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            fs.Write(buffer, 0, read);
                            bytesRead += read;
                            OnFtpProgress(new FtpProgressEventArgs(remotePath, localPath, (double)bytesRead / (double)bytesTotal));
                        }
                    }
                }
                response.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public override bool FileExists(string remotePath)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(Hostname.TrimEnd('/') + "/" + remotePath, UriKind.RelativeOrAbsolute));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = Credentials;
            request.KeepAlive = false;
            request.UseBinary = true;
            request.UsePassive = true;

            request.Method = WebRequestMethods.Ftp.GetDateTimestamp;

            try
            {
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                response.Close();
                return true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    return false;
                }
                return true;
            }
        }

        public override bool DirectoryExists(string remotePath)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(Hostname.TrimEnd('/') + "/" + remotePath, UriKind.RelativeOrAbsolute));
            request.Method = WebRequestMethods.Ftp.MakeDirectory;
            request.Credentials = Credentials;
            request.KeepAlive = false;
            request.UseBinary = true;
            request.UsePassive = true;

            try
            {
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream ftpStream = response.GetResponseStream();
                ftpStream.Close();
                response.Close();
                return true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    return false;
                }
                return true;
            }
        }

        public override bool CreateDirectory(string remotePath)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(Hostname.TrimEnd('/') + "/" + remotePath, UriKind.RelativeOrAbsolute));
            request.Method = WebRequestMethods.Ftp.MakeDirectory;
            request.Credentials = Credentials;
            request.KeepAlive = false;
            request.UseBinary = true;
            request.UsePassive = true;

            try
            {
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream ftpStream = response.GetResponseStream();
                ftpStream.Close();
                response.Close();
                return true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                return false;
            }
        }

        public override bool Rename(string remotePath, string renameTo)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(Hostname.TrimEnd('/') + "/" + remotePath, UriKind.RelativeOrAbsolute));
            request.Method = WebRequestMethods.Ftp.Rename;
            request.Credentials = Credentials;
            request.KeepAlive = false;
            request.UseBinary = true;
            request.UsePassive = true;
            request.RenameTo = renameTo;

            request.Method = WebRequestMethods.Ftp.Rename;

            try
            {
                if (FileExists(renameTo)) DeleteFile(renameTo);

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                response.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public override bool DeleteFile(string remotePath)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(Hostname.TrimEnd('/') + "/" + remotePath, UriKind.RelativeOrAbsolute));
            request.Method = WebRequestMethods.Ftp.DeleteFile;
            request.Credentials = Credentials;

            request.Method = WebRequestMethods.Ftp.DeleteFile;

            try
            {
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                response.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public override IEnumerable<string> FindFiles(string remotePath, string wildcards)
        {
            List<string> output = new List<string>(); ;

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(Hostname.TrimEnd('/') + "/" + remotePath, UriKind.RelativeOrAbsolute));
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.Credentials = Credentials;

            try
            {
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                using (StreamReader s = new StreamReader(response.GetResponseStream()))
                {
                    while (!s.EndOfStream)
                    {
                        string line = s.ReadLine();
                        if (line != "." || line != "..") continue;
                        if (Regex.IsMatch(line, "^" + Regex.Escape(wildcards).Replace("\\*", ".*") + "$"))
                        {
                            output.Add(line);
                        }
                    }
                }
                response.Close();
            }
            catch
            {
            }

            return output;
        }
    }

    public class Sftp : Ftp
    {
        ConnectionInfo connectionInfo;
        SftpClient client;

        public Sftp(string hostname, NetworkCredential credentials) : base(hostname, credentials)
        {
            connectionInfo = new ConnectionInfo(Hostname, Credentials.UserName, new PasswordAuthenticationMethod(Credentials.UserName, Credentials.Password));
            client = new SftpClient(connectionInfo);
            client.Connect();
        }

        public override object Deserialize(string remotePath)
        {
            using (Stream s = client.OpenRead(remotePath))
            {
                BinaryFormatter bf = new BinaryFormatter();
                return bf.Deserialize(s);
            }
        }

        public override bool Serialize(string remotePath, object obj, bool safely = false, int backups = 0)
        {
            string uri = remotePath;
            try
            {
                if (safely)
                {
                    uri = GetSafeTempPath(uri);
                }

                using (Stream s = client.OpenWrite(remotePath))
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    bf.Serialize(s, obj);
                }

                // Uploaded safely
                if (safely)
                {
                    if (backups > 0) Rename(remotePath, remotePath.Substring(remotePath.LastIndexOf('/') + 1) + ".BACKUP_" + DateTime.Now.ToString("yyyyMMddHHmmss", CultureInfo.InvariantCulture));
                    Rename(uri, remotePath.Substring(remotePath.LastIndexOf('/') + 1));
                    CleanUpBackups(remotePath, backups);
                }
                return true;
            }
            catch
            {
                DeleteFile(uri);
                return false;
            }
        }

        public override bool UploadFile(string remotePath, string localPath, bool safely = false, int backups = 0)
        {
            string uri = remotePath;
            try
            {
                if (safely)
                {
                    int index = 0;
                    while (FileExists(uri + ".TEMPORARY_" + index))
                    {
                        index++;
                    }
                    uri += ".TEMPORARY_" + index;
                }

                using (Stream s = client.OpenWrite(remotePath))
                {
                    using (FileStream fs = File.Open(localPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        int bytesRead = 0;
                        int bytesTotal = (int)fs.Length;

                        byte[] buffer = new byte[1024];
                        int read;
                        while ((read = fs.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            s.Write(buffer, 0, read);
                            bytesRead += read;
                            OnFtpProgress(new FtpProgressEventArgs(remotePath, localPath, (double)bytesRead / (double)bytesTotal));
                        }
                    }
                }

                // Uploaded safely
                if (safely)
                {
                    if (backups > 0) Rename(remotePath, remotePath.Substring(remotePath.LastIndexOf('/') + 1) + ".BACKUP_" + DateTime.Now.ToString("yyyyMMddHHmmss", CultureInfo.InvariantCulture));
                    Rename(uri, remotePath.Substring(remotePath.LastIndexOf('/') + 1));
                    CleanUpBackups(remotePath, backups);
                }
                return true;
            }
            catch
            {
                DeleteFile(uri);
                return false;
            }
        }

        public override bool DownloadFile(string remotePath, string localPath)
        {
            try
            {
                using (Stream s = client.OpenRead(remotePath))
                {
                    using (FileStream fs = File.Create(localPath))
                    {
                        int bytesRead = 0;
                        int bytesTotal = (int)s.Length;

                        byte[] buffer = new byte[1024];
                        int read;
                        while ((read = s.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            fs.Write(buffer, 0, read);
                            bytesRead += read;
                            OnFtpProgress(new FtpProgressEventArgs(remotePath, localPath, (double)bytesRead / (double)bytesTotal));
                        }
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public override bool FileExists(string remotePath)
        {
            try
            {
                client.GetLastWriteTime(remotePath);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public override bool DirectoryExists(string remotePath)
        {
            try
            {
                client.GetLastWriteTime(remotePath);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public override bool CreateDirectory(string remotePath)
        {
            try
            {
                client.CreateDirectory(remotePath);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public override bool Rename(string remotePath, string renameTo)
        {
            try
            {
                renameTo = remotePath.Substring(0, remotePath.LastIndexOf('/')) + "/" + renameTo;
                client.RenameFile(remotePath, renameTo);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public override bool DeleteFile(string remotePath)
        {
            try
            {
                client.DeleteFile(remotePath);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public override IEnumerable<string> FindFiles(string remotePath, string wildcards)
        {
            List<string> output = new List<string>();

            try
            {
                var files = client.ListDirectory(remotePath);
                foreach (var f in files.Skip(2)) // skip "." and ".."
                {
                    if (Regex.IsMatch(f.Name, "^" + Regex.Escape(wildcards).Replace("\\*", ".*") + "$"))
                    {
                        output.Add(f.Name);
                    }
                }
            }
            catch
            {
            }

            return output;
        }
    }
}
