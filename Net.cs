﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Net
{
    public enum HttpProcess { DownloadingFile, UploadingFile }

    public static class Http
    {
        public static string UserAgent { get; set; } = "Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko";

        public class FileDownload
        {
            public string RemotePath { get; private set; }
            public string LocalPath { get; private set; }

            public FileDownload(string remotePath, string localPath = null)
            {
                RemotePath = remotePath;
                LocalPath = localPath ?? Path.GetTempFileName();
                Directory.CreateDirectory(Path.GetDirectoryName(LocalPath));
            }

            public void Download()
            {
                WebClient client = new WebClient
                {
                    UseDefaultCredentials = true,
                    Proxy = WebRequest.GetSystemWebProxy()
                };
                if (client.Proxy != null) client.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                client.Headers.Add("user-agent", UserAgent);

                client.DownloadProgressChanged += Client_DownloadProgressChanged;
                client.DownloadFile(new Uri(RemotePath), LocalPath);
            }

            public async Task DownloadAsync()
            {
                WebClient client = new WebClient
                {
                    UseDefaultCredentials = true,
                    Proxy = WebRequest.GetSystemWebProxy()
                };
                if (client.Proxy != null) client.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                client.Headers.Add("user-agent", UserAgent);

                client.DownloadProgressChanged += Client_DownloadProgressChanged;
                await client.DownloadFileTaskAsync(new Uri(RemotePath), LocalPath);
            }

            private void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
            {
                OnFileDownloadProgress(new FileDownloadProgressEventArgs(RemotePath, LocalPath, (double)e.BytesReceived / (double)e.TotalBytesToReceive));
            }

            public delegate void FileDownloadProgressHandler(FileDownloadProgressEventArgs e);
            public event FileDownloadProgressHandler FileDownloadProgress;
            void OnFileDownloadProgress(FileDownloadProgressEventArgs e)
            {
                FileDownloadProgress?.Invoke(e);
            }

            public class FileDownloadProgressEventArgs : EventArgs
            {
                public string RemotePath { get; private set; }
                public string LocalPath { get; private set; }
                public double Progress { get; private set; }

                public FileDownloadProgressEventArgs(string remotePath, string localPath, double progress) : base()
                {
                    RemotePath = remotePath;
                    LocalPath = localPath;
                    Progress = progress;
                }
            }
        }

        public static string DownloadFile(string url, string path = "")
        {
            WebClient client = new WebClient
            {
                UseDefaultCredentials = true,
                Proxy = WebRequest.GetSystemWebProxy()
            };
            if (client.Proxy != null) client.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
            client.Headers.Add("user-agent", UserAgent);

            if (string.IsNullOrEmpty(path))
                path = Path.GetTempFileName();

            client.DownloadFile(url, path);

            return path;
        }

        public static string DownloadWebPage(string url)
        {
            WebClient client = new WebClient
            {
                UseDefaultCredentials = true,
                Proxy = WebRequest.GetSystemWebProxy()
            };
            if (client.Proxy != null) client.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
            client.Headers.Add("user-agent", UserAgent);

            string result;
            using (Stream data = client.OpenRead(url))
            {
                using (StreamReader reader = new StreamReader(data))
                {
                    result = reader.ReadToEnd();
                }
            }
            return result;
        }

        public static string DownloadPost(string baseAddress, string requestUri, List<KeyValuePair<string, string>> headers, List<KeyValuePair<string, string>> formData, bool unzip)
        {
            System.Net.WebClient client = new System.Net.WebClient
            {
                UseDefaultCredentials = true,
                Proxy = WebRequest.GetSystemWebProxy()
            };
            if (client.Proxy != null) client.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
            string result = "";

            client.BaseAddress = baseAddress;

            if (headers == null) headers = new List<KeyValuePair<string, string>>();
            foreach (var header in headers)
            {
                if (header.Key == "Host") continue;
                client.Headers.Add(header.Key, header.Value); // request.Headers.Add(header.Key, header.Value);
            }

            if (formData == null) formData = new List<KeyValuePair<string, string>>();
            string query = "";
            if (formData != null)
            {
                // Separate the KeyValuePairs in to a query string
                foreach (var pair in formData)
                {
                    if (query.Length != 0)
                    {
                        query += "&";
                    }

                    query += pair.Key + "=" + pair.Value;
                }
            }

            // Make the request
            var reqparm = new System.Collections.Specialized.NameValueCollection();
            foreach (var item in formData)
            {
                reqparm.Add(item.Key, item.Value);
            }
            byte[] responsebytes = client.UploadValues(baseAddress + requestUri, "POST", reqparm);

            if (unzip && IsGZip(responsebytes))
            {
                result = UnZip(responsebytes);
            }
            else
            {
                result = UTF8Encoding.UTF8.GetString(responsebytes);
            }

            return result;
        }

        public static string DownloadGet(string address, List<KeyValuePair<string, string>> headers, bool binary, bool unzip)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(address);
            request.UseDefaultCredentials = true;
            request.Proxy = WebRequest.GetSystemWebProxy();
            request.Method = "GET";
            request.UserAgent = UserAgent;
            if (request.Proxy != null) request.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
            foreach (var header in headers)
            {
                switch (header.Key.ToLower())
                {
                    case "accept": request.Accept = header.Value; break;
                    case "referer": request.Referer = header.Value; break;
                    case "host": request.Host = header.Value; break;
                    default: request.Headers.Add(header.Key, header.Value); break;
                }
            }
            string textResult;
            byte[] binaryResult;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (binary)
                {
                    Stream dataStream = response.GetResponseStream();
                    BinaryReader br = new BinaryReader(dataStream);
                    const int bufferSize = 4096;
                    using (var ms = new MemoryStream())
                    {
                        byte[] buffer = new byte[bufferSize];
                        int count;
                        while ((count = br.Read(buffer, 0, buffer.Length)) != 0)
                            ms.Write(buffer, 0, count);
                        binaryResult = ms.ToArray();
                    }
                    br.Close();
                    dataStream.Close();

                    if (unzip && IsGZip(binaryResult))
                    {
                        textResult = UnZip(binaryResult);
                    }
                    else
                    {
                        textResult = Encoding.UTF8.GetString(binaryResult);
                    }
                    return textResult;
                }
                else
                {
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    textResult = reader.ReadToEnd();
                    reader.Close();
                    dataStream.Close();

                    if (unzip)
                    {
                        textResult = UnZip(textResult);
                    }
                    return textResult;
                }
            }
        }

        //private static byte[] ReadAllBytes(this BinaryReader reader)
        //{
        //    const int bufferSize = 4096;
        //    using (var ms = new MemoryStream())
        //    {
        //        byte[] buffer = new byte[bufferSize];
        //        int count;
        //        while ((count = reader.Read(buffer, 0, buffer.Length)) != 0)
        //            ms.Write(buffer, 0, count);
        //        return ms.ToArray();
        //    }
        //}

        private static bool IsGZip(Stream stream)
        {
            byte[] arr = new byte[2];
            int bytesRead = stream.Read(arr, 0, 2);
            stream.Seek(-2, SeekOrigin.Current);
            if (bytesRead < 2) return false;
            return IsGZip(arr);
        }

        private static bool IsGZip(byte[] bytes)
        {
            if (bytes.Length < 2) return false;
            return bytes[0] == 31 && bytes[1] == 139;
        }

        public static string UnZip(Stream s)
        {
            StringBuilder sb = new StringBuilder();
            using (System.IO.Compression.GZipStream sr = new System.IO.Compression.GZipStream(s, System.IO.Compression.CompressionMode.Decompress))
            {
                const int size = 4096;
                byte[] buffer = new byte[size];

                int count = 0;
                do
                {
                    count = sr.Read(buffer, 0, size);
                    for (int i = 0; i < count; i++)
                    {
                        sb.Append((char)buffer[i]);
                    }
                }
                while (count > 0);
            }
            return sb.ToString();
        }

        public static string UnZip(string value)
        {
            byte[] byteArray = new byte[value.Length]; //Transform string into byte[]
            int indexBA = 0;
            foreach (char item in value.ToCharArray())
            {
                byteArray[indexBA++] = (byte)item;
            }

            return UnZip(byteArray);
        }

        public static string UnZip(byte[] value)
        {
            using (System.IO.MemoryStream msInput = new System.IO.MemoryStream(value))
            {
                using (System.IO.Compression.GZipStream sr = new System.IO.Compression.GZipStream(msInput, System.IO.Compression.CompressionMode.Decompress))
                {
                    using (System.IO.MemoryStream msOutput = new System.IO.MemoryStream())
                    {
                        sr.CopyTo(msOutput);
                        return Encoding.UTF8.GetString(msOutput.ToArray());
                    }
                }
            }
        }
    }
}
