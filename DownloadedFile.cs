﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Net
{
    public class DownloadedFile : IDisposable
    {
        public string RemotePath { get; set; } = "";
        public string Path { get; set; } = "";
        private bool isDownloaded;

        private Stream stream;

        public DownloadedFile(string remotePath, string dirname)
        {
            this.RemotePath = remotePath;
            Path = "";
            if (string.IsNullOrEmpty(dirname))
                Path = System.IO.Path.Combine(System.IO.Path.GetTempPath(), Guid.NewGuid().ToString());
            else
                Path = System.IO.Path.Combine(System.IO.Path.GetTempPath(), dirname, Guid.NewGuid().ToString());
        }

        public void Download()
        {
            try
            {
                if (!Directory.Exists(System.IO.Path.GetDirectoryName(Path)))
                {
                    Directory.CreateDirectory(System.IO.Path.GetDirectoryName(Path));
                }
                Net.Http.DownloadFile(RemotePath, Path);
                isDownloaded = true;
            }
            catch
            {
                isDownloaded = false;
            }
        }

        public Stream OpenAsStream()
        {
            if (!isDownloaded) throw new IOException("An attempt was made to open a file that was not downloaded yet. Make call to Download() method first.");
            stream = File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            return stream;
        }

        public byte[] GetBytes()
        {
            if (!isDownloaded) throw new IOException("An attempt was made to open a file that was not downloaded yet. Make call to Download() method first.");
            using (BinaryReader br = new BinaryReader(File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                const int bufferSize = 4096;
                using (var ms = new MemoryStream())
                {
                    byte[] buffer = new byte[bufferSize];
                    int count;
                    while ((count = br.Read(buffer, 0, buffer.Length)) != 0)
                        ms.Write(buffer, 0, count);
                    return ms.ToArray();
                }
            }
        }

        public string GetString()
        {
            if (!isDownloaded) throw new IOException("An attempt was made to open a file that was not downloaded yet. Make call to Download() method first.");
            using (StreamReader sr = new StreamReader(File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                return sr.ReadToEnd();
            }
        }

        public IEnumerable<string> GetLines()
        {
            if (!isDownloaded) throw new IOException("An attempt was made to open a file that was not downloaded yet. Make call to Download() method first.");
            using (StreamReader sr = new StreamReader(File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                while (!sr.EndOfStream)
                {
                    yield return sr.ReadLine();
                }
            }
        }

        public void Dispose()
        {
            if (stream!=null && stream.CanRead)
            {
                stream.Close();
            }
            stream = null;

            try
            {
                File.Delete(Path);
            }
            catch { }
        }
    }
}
