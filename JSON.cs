﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Budziszewski.Net
{
    public class JSON
    {
        public JSON(string source)
        {
            JsonTextReader reader = new JsonTextReader(new StringReader(source));
            List<KeyValuePair<JsonToken, object>> json = new List<KeyValuePair<JsonToken, object>>();

            // Read the data
            while (reader.Read())
            {
                json.Add(new KeyValuePair<JsonToken, object>(reader.TokenType, reader.Value));
            }

            // Interpret the data
            Root = new JSONObject(json);
        }

        public JSONObject Root { get; set; }
    }

    public class JSONObject
    {
        public JSONObject()
        {

        }

        public JSONObject(List<KeyValuePair<JsonToken, object>> json) : this(json, 0, json.Count)
        {

        }

        public JSONObject(List<KeyValuePair<JsonToken, object>> json, int begin, int end)
        {
            int delimiter = -1;
            Length = end - begin;

            for (int i = begin; i < end; i++)
            {
                switch (json[i].Key)
                {
                    case JsonToken.None: break;
                    case JsonToken.StartObject:
                        delimiter = FindMatchingDelimiter(json, i);
                        Children.Add(new JSONObject(json, i + 1, delimiter));
                        i = delimiter;
                        break;
                    case JsonToken.StartArray:
                        delimiter = FindMatchingDelimiter(json, i);
                        Children.Add(new JSONArray(json, i + 1, delimiter));
                        i = delimiter;
                        break;
                    case JsonToken.StartConstructor: break;
                    case JsonToken.PropertyName:
                        Children.Add(new JSONAttribute(json[i].Value.ToString(), json[i + 1].Value));
                        i++;
                        break;
                    case JsonToken.Comment: break;
                    case JsonToken.Raw: break;
                    case JsonToken.Integer:
                    case JsonToken.Float:
                    case JsonToken.String:
                    case JsonToken.Boolean:
                        Children.Add(new JSONValue(json[i].Value));
                        break;
                    case JsonToken.Null: break;
                    case JsonToken.Undefined: break;
                    case JsonToken.EndObject: break;
                    case JsonToken.EndArray: break;
                    case JsonToken.EndConstructor: break;
                    case JsonToken.Date: break;
                    case JsonToken.Bytes: break;
                    default: break;
                }
            }
        }

        public List<JSONObject> Children { get; set; } = new List<JSONObject>();
        public int Length { get; protected set; }

        public object GetAttributeValue(string key)
        {
            JSONAttribute obj = Children.FirstOrDefault(x => x is JSONAttribute && (x as JSONAttribute).Token == key) as JSONAttribute;
            if (obj == null) return null;
            return obj.Value;
        }

        public override string ToString()
        {
            return String.Format("JSONObject: {0} children, length: {1}", Children.Count, Length);
        }

        private static int FindMatchingDelimiter(List<KeyValuePair<JsonToken, object>> json, int begin)
        {
            JsonToken openingDel = json[begin].Key;
            JsonToken closingDel;
            switch (openingDel)
            {
                case JsonToken.StartObject: closingDel = JsonToken.EndObject; break;
                case JsonToken.StartArray: closingDel = JsonToken.EndArray; break;
                case JsonToken.StartConstructor: closingDel = JsonToken.EndConstructor; break;
                default: return -1;
            }

            int level = 1;

            for (int i = begin + 1; i < json.Count; i++)
            {
                if (json[i].Key == openingDel) level++;
                if (json[i].Key == closingDel) level--;
                if (level == 0) return i;
            }

            return -1;
        }
    }

    public class JSONAttribute : JSONObject
    {
        public string Token { get; set; }
        public object Value { get; set; }

        public JSONAttribute(string token, object value)
        {
            Token = token;
            Value = value;
            Length = 1;
        }

        public override string ToString()
        {
            return String.Format("{0}: {1}", Token, Value);
        }
    }

    public class JSONValue : JSONObject
    {
        public object Value { get; set; }

        public JSONValue(object value)
        {
            Value = value;
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }

    public class JSONArray : JSONObject
    {
        public JSONArray() : base()
        {

        }

        public JSONArray(List<KeyValuePair<JsonToken, object>> json) : this(json, 0, json.Count)
        {

        }

        public JSONArray(List<KeyValuePair<JsonToken, object>> json, int begin, int end) : base(json, begin, end)
        {

        }

        public override string ToString()
        {
            return String.Format("JSONArray: {0} children, length: {1}", Children.Count, Length);
        }
    }
}
